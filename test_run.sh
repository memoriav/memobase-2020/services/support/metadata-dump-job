

export KAFKA_CONNECTION_RETRIES=0
export TOPIC_IN="mb-di-processed-records-prod"
export KAFKA_BOOTSTRAP_SERVERS="localhost:8080"
export GROUP_ID="test-export-1"

python src/run.py