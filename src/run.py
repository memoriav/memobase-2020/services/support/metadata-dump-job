#  metadata-dump-job
#  Copyright (C) 2020  Memoriav
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published
#  by the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
import bz2
import logging
import os
import sys
import time
from typing import Union

import paramiko
from kafka import KafkaConsumer
from kafka.errors import KafkaError

logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format='[%(levelname)s] [%(module)s:%(lineno)d] %(message)s')


def connect_to_kafka(retries=0):
    """
    Connect to Kafka. Abort after configured retries.
    """
    try:
        return KafkaConsumer(
            os.environ["TOPIC_IN"],
            key_deserializer=lambda k: k.decode("utf8"),
            bootstrap_servers=os.environ["KAFKA_BOOTSTRAP_SERVERS"],
            auto_offset_reset="earliest",
            enable_auto_commit=False,
            group_id=os.environ["GROUP_ID"],
            consumer_timeout_ms=30000,
        )
    except KafkaError as ex:
        status = "KafkaError: " + str(ex)
        logging.error(status)
        if retries < int(os.environ["KAFKA_CONNECTION_RETRIES"]):
            time.sleep(30 * (retries + 1))
            connect_to_kafka(retries + 1)
        return None
    except Exception as ex:
        status = "Exception: " + str(ex)
        logging.error(status)
        if retries < int(os.environ["KAFKA_CONNECTION_RETRIES"]):
            time.sleep(30 * (retries + 1))
            connect_to_kafka(retries + 1)
        return None


def connect_to_sftp(hostname: str, port: int, username: str, password: str):
    t = paramiko.Transport((hostname, port))
    t.connect(username=username, password=password)
    return paramiko.SFTPClient.from_transport(t), t


if __name__ == '__main__':
    env = os.environ['ENV']
    encoded_newline = '\n'.encode('utf-8')
    consumer: Union[KafkaConsumer, None] = connect_to_kafka()
    if consumer is not None:
        logging.info(
            'Established consumer from Kafka and reading from {}.'.format(os.environ["TOPIC_IN"]))
        try:
            sftp, socket = connect_to_sftp(os.environ['SFTP_HOST'],
                                           int(os.environ['SFTP_PORT']),
                                           os.environ['SFTP_USER'],
                                           os.environ['SFTP_PASSWORD'])
            logging.info('Successfully connected to sftp socket. Begin writing file...')
            comp = bz2.BZ2Compressor()
            with sftp.open(f'dumps/memobase-metadata-dump-json-lines-{env}.txt.bz2', 'wb') as fp:
                for record in consumer:
                    message = record.value
                    fp.write(comp.compress(message + encoded_newline))
                fp.write(comp.flush())
            logging.info('Finished writing file.')
        finally:
            sftp.close()
            socket.close()
